"""Mod. fondos

Revision ID: 2bd50a1056af
Revises: 603c1884df2e
Create Date: 2020-05-15 02:12:53.067535

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '2bd50a1056af'
down_revision = '603c1884df2e'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('fondo', sa.Column('categoria', sa.String(length=50), nullable=False))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('fondo', 'categoria')
    # ### end Alembic commands ###
