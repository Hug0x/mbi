var connect = require('connect');
var serveStatic = require('serve-static');
connect().use(serveStatic(__dirname)).listen(8050, function(){
    console.log('Server running on 8050... http://localhost:8050');
});