from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileAllowed
from wtforms import StringField, PasswordField, SubmitField, BooleanField, TextAreaField, SelectField, DateField, IntegerField, TimeField
from wtforms.validators import DataRequired, Length, Email, EqualTo, ValidationError
from flask_login import current_user
from MBI.models import texto, User, documento, fondo



class formtexto(FlaskForm):
    titulo = StringField('Titulo', validators=[DataRequired()])
    seccion = StringField('Seccion', validators=[DataRequired()])
    subseccion = StringField('Subseccion', validators=[Length(max=50, message=("Debe ser entre 0 y 50 caracteres de largo"))] )
    contenido = TextAreaField('Contenido', validators=[DataRequired()])
    submit = SubmitField('Editar')

class formLogin(FlaskForm):
    usuario = StringField('Usuario', validators=[DataRequired()])
    contraseña = PasswordField('Contraseña', validators=[DataRequired()])
    recordar = BooleanField('Recordarme')
    submit = SubmitField('Login')

class formFP(FlaskForm):
    email = StringField('Email')
    submit = SubmitField('Login')

class formAccount(FlaskForm):
    nombre = StringField('Nombre', validators=[DataRequired(), Length(min=2, max=20, message=("Debe ser entre 2 y 20 caracteres de largo")) ])
    apellido = StringField('Apellido', validators=[DataRequired(), Length(min=2, max=20, message=("Debe ser entre 2 y 20 caracteres de largo"))])
    username = StringField('Username', validators=[DataRequired(), Length(min=4, max=20, message=("Debe ser entre 4 y 20 caracteres de largo"))])
    email = StringField('Email', validators=[DataRequired(), Email()])
    contraseña = PasswordField('Contraseña', validators=[DataRequired(), Length(min=4, max=15, message=("Debe ser entre 4 y 15 caracteres de largo"))])
    conf_contraseña = PasswordField('Confirmar', validators=[DataRequired(), EqualTo('contraseña', message="Las contraseñas deben ser iguales")])
    submit = SubmitField('Registrarse')

    class Meta:
        locales = ['es_ES', 'es']

    def validate_username(self, username):
        user = User.query.filter_by(username=username.data).first()
        if user:
            raise ValidationError('Este nombre de usuario ya existe, elija otro')

    def validate_email(self, email):
        user = User.query.filter_by(email=email.data).first()
        if user:
            raise ValidationError('El correo ya se encuentra registrado, seleccione otro')

class formArchivo(FlaskForm):
    tipo = SelectField(coerce=int)
    nombre = StringField("Nombre Archivo", validators=[DataRequired()])
    fondo = SelectField(coerce=int)
    archivo = FileField("Elija un Archivo",validators=[FileAllowed(['pdf'])])
    archivo2 = FileField("Elija un Archivo")
    submit = SubmitField("Subir")

    def validate_file(self, nombre, tipo, fondo):
        print("val form")
        doc = documento.query.filter_by(nombre=nombre.data, tipo=tipo.data, fondo=fondo.data).first()
        if doc:
            raise ValidationError('El archivo ya existe, elija otro')

class formAccountCorredora(FlaskForm):
    rut = StringField('RUT', validators=[DataRequired()])
    nombre = StringField('Nombre', validators=[DataRequired(), Length(min=2, max=20, message=("Debe ser entre 2 y 20 caracteres de largo")) ])
    apellidoP = StringField('Apellido Paterno', validators=[DataRequired(), Length(min=2, max=20, message=("Debe ser entre 2 y 20 caracteres de largo"))])
    apellidoM = StringField('Apellido Materno', validators=[DataRequired(), Length(min=2, max=20, message=("Debe ser entre 2 y 20 caracteres de largo"))])
    fecha_nac = DateField("Fecha de Nacimiento")
    nacionalidad = StringField('Nacionalidad')
    estado = SelectField("Estado Civil", coerce=str)
    direccion = StringField('Direccion')
    comuna = StringField('Comuna')
    ciudad = StringField('Ciudad')
    pais = StringField('Pais')
    telefono_f = StringField('Telefono Fijo')
    telefono_m = StringField('Telefono Movil')
    email = StringField('Email', validators=[DataRequired(), Email()])
    profesion = StringField('Profesion')
    cargo = StringField('Cargo')
    empleador = StringField('Empleador')
    rut_emp = StringField('RUT Empleador')
    banco = StringField('Banco')
    cuenta = StringField('N° Cta. Corriente')
    submit = SubmitField('Enviar')

class formNoticia(FlaskForm):
    tipo =  SelectField("Tipo de Noticia", coerce=str, choices=[("mbi-prensa","MBI en la prensa (quienes somos)"),("inicio","Comunicacion a Clientes (noticias home)"),("bolsa","Publicaciones Corredora")] )
    imagen = FileField("Elija una Imagen",validators=[FileAllowed(['png','jpeg'])])
    archivo = FileField("Elija un Archivo",validators=[FileAllowed(['pdf'])])
    titulo = StringField('Titulo', validators=[DataRequired(), Length(min=10, max=150, message=("Debe ser entre 10 y 150 caracteres de largo"))])
    bajada = StringField('Bajada - Resumen', validators=[Length(max=255, message=("Debe ser de un maximo de 250 caracteres de largo"))])
    contenido = TextAreaField('Contenido')
    fecha =  DateField("Fecha de Publicacion", validators=[DataRequired()])
    submit = SubmitField('Subir Noticia')

class formLegalesAGF(FlaskForm):
    memorias = SelectField(coerce=str)
    eeff = SelectField(coerce=str)
    manuales = SelectField(coerce=str)
    otros = SelectField(coerce=str)

class formContacto(FlaskForm):
    nombre = StringField('Nombre', validators=[DataRequired()])
    email = StringField('Email', validators=[DataRequired(), Email()])
    asunto = StringField('Asunto', validators=[DataRequired()])
    mensaje = TextAreaField('Mensaje', validators=[DataRequired(), Length(max=1000, message=("Debe ser de un maximo de 1000 caracteres de largo"))])
    submit = SubmitField('Enviar')

class formFondo(FlaskForm):
    nombre = StringField('Nombre Fondo', validators=[DataRequired(), Length(min=2, max=255, message=("Debe ser entre 2 y 255 caracteres de largo"))])
    nemo = StringField('Nemotecnico', validators=[DataRequired(), Length(min=1, max=20, message=("Debe ser entre 1 y 20 caracteres de largo"))])
    ruta = StringField('Ruta Web', validators=[DataRequired(), Length(min=1, max=50, message=("Debe ser entre 1 y 50 caracteres de largo"))])
    submit = SubmitField('Enviar')

    def validate_nombre(self, nombre):
        nf = str(nombre.data).upper()
        dato = fondo.query.filter_by(nombre_fondo=nf).first()
        if dato:
            raise ValidationError('Este nombre de fondo ya existe, elija otro')
    
    def validate_ruta(self, ruta):
        r = str(ruta.data).lower()
        dato = fondo.query.filter_by(ruta_web=r).first()
        if dato:
            raise ValidationError('La ruta para la pagina ya se encuentra registrada, seleccione otra')

    def validate_nemo(self, nemo):
        n = str(nemo.data).upper()
        dato = fondo.query.filter_by(nemo=n).first()
        if dato:
            raise ValidationError('El nemotecnico ya se encuentra registrado, seleccione otro')

class formDocFondo(FlaskForm):
    factsheet = SelectField(coerce=str)
    eeff = SelectField(coerce=str)
    folleto = SelectField(coerce=str)
    informacion = SelectField(coerce=str)
    #reglamento = SelectField(coerce=int)

class formLoginWebinar(FlaskForm):
    correo = StringField('Email', validators=[DataRequired(), Email()])
    password = StringField('RUT, sin puntos ni guion', validators=[DataRequired()])
    submit = SubmitField('Enviar')

class formWebinar(FlaskForm):
    nombre = StringField('Nombre Webinar', validators=[DataRequired(), Length(min=2, max=255, message=("Debe ser entre 2 y 255 caracteres de largo"))])
    resumen = StringField('Resumen Webinar', validators=[Length(max=300, message=("Debe ser de un maximo de 300 caracteres de largo"))])
    descripcion = TextAreaField('Descripcion Webinar', validators=[Length(max=2000, message=("Debe ser de un maximo de 2000 caracteres de largo"))])
    #link_page = StringField('Nombre link a compartir', validators=[DataRequired(), Length(min=2, max=255, message=("Debe ser entre 2 y 255 caracteres de largo"))])
    link_youtube = StringField('Link Youtube')#, validators=[DataRequired(), Length(min=2, max=255, message=("Debe ser entre 2 y 255 caracteres de largo"))])
    chat = BooleanField('Webinar con chat?')
    privado = BooleanField('Webinar privado?')
    preguntas = BooleanField('Habilitar preguntas')
    fecha =  DateField("Fecha de Publicacion", validators=[DataRequired()])
    hora = TimeField("Hora del webinar", validators=[DataRequired()])
    archivo = FileField("Elija un Archivo", validators=[FileAllowed(['csv','xls','xlsx'])])
    correos = StringField('Destinatario Correo', validators=[Email()])
    telefonos = TextAreaField('Destinatarios Whatsapp (separados por comas)', validators=[Length(max=2000, message=("Debe ser de un maximo de 2000 caracteres de largo"))])
    submit = SubmitField('Guardar')

class formSpeaker(FlaskForm):
    nombre = StringField('Nombre Webinar', validators=[DataRequired(), Length(min=2, max=255, message=("Debe ser entre 2 y 255 caracteres de largo"))])
    tipo = IntegerField()
    foto = FileField("Elija una Imagen", validators=[FileAllowed(['jpg','jpeg','png'])])
    #cargo = StringField('Nombre cargo', validators=[DataRequired(), Length(min=2, max=255, message=("Debe ser entre 2 y 255 caracteres de largo"))])
    reseña = TextAreaField('Reseña', validators=[DataRequired(), Length(max=500, message=("Debe ser de un maximo de 500 caracteres de largo"))])
    submit = SubmitField('Guardar')

class formLoginWebinar2(FlaskForm):
    nombre = StringField('Tu nombre', validators=[DataRequired()])
    apellido = StringField('Tu apellido', validators=[DataRequired()])
    correo = StringField('Email', validators=[DataRequired(), Email()])
    rut = StringField('RUT, sin puntos ni guion', validators=[DataRequired()])
    submit = SubmitField('Enviar')

class formPreguntaWebinar(FlaskForm):
    #nombre = StringField('Tu nombre', validators=[DataRequired()])
    pregunta = TextAreaField('Escribe tu pregunta', validators=[DataRequired(), Length(max=200, message=("Debe ser de un maximo de 200 caracteres de largo"))])
    submit = SubmitField('Enviar')