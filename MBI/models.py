from datetime import datetime
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from flask import current_app
from MBI import db, login_manager
from flask_login import UserMixin


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))


class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    nombre = db.Column(db.String(20), unique=False, nullable=False)
    apellido = db.Column(db.String(20), unique=False, nullable=False)
    username = db.Column(db.String(20), unique=True, nullable=False)
    email = db.Column(db.String(120), unique=True, nullable=False)
    password = db.Column(db.String(100), nullable=False)
    tipo_user = db.Column(db.Integer,nullable= False)
    def get_reset_token(self, expires_sec=1800):
        s = Serializer(current_app.config['SECRET_KEY'], expires_sec)
        return s.dumps({'user_id': self.id}).decode('utf-8')

    @staticmethod
    def verify_reset_token(token):
        s = Serializer(current_app.config['SECRET_KEY'])
        try:
            user_id = s.loads(token)['user_id']
        except:
            return None
        return User.query.get(user_id)

    def __repr__(self):
        return {self.nombre}+{self.apellido}



class texto(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    titulo = db.Column(db.String(100), nullable=False)
    seccion = db.Column(db.String(50), nullable=False)
    subseccion = db.Column(db.String(50), nullable=True)
    contenido = db.Column(db.Text, nullable=False)
    idioma = db.Column(db.String(50), nullable=False)
'''
class documentoFondo(db.Model):
    id = db.Column(db.Integer, primary_key= True)
    nombre = db.Column(db.String(255), nullable=False)
    tipo = db.Column(db.Integer, db.ForeignKey('tipos_archivo.id'), nullable=False)
    fondo = db.Column(db.Integer, db.ForeignKey('fondo.id'), nullable=False)

    __table_args__ = (db.UniqueConstraint('nombre','tipo','fondo', name = 'ui_doc'),)
'''
class documento(db.Model):
    id = db.Column(db.Integer, primary_key= True)
    nombre = db.Column(db.String(255), nullable=False)
    tipo = db.Column(db.Integer, db.ForeignKey('tipos_archivo.id'), nullable=False)
    nom_int = db.Column(db.String(255), nullable=False)
    link = db.Column(db.String(255), nullable=False)
    fondo = db.Column(db.Integer, db.ForeignKey('fondo.id'), nullable=True)
    visible = db.Column(db.Boolean,nullable = False, default=1)

    __table_args__ = (db.UniqueConstraint('nombre','tipo','nom_int', name = 'ui_doc'),)

class fondo(db.Model):
    id = db.Column(db.Integer, primary_key= True)
    nombre_fondo = db.Column(db.String(255), nullable=False, unique = True)
    nemo = db.Column(db.String(20), nullable=False, unique = True)
    ruta_web = db.Column(db.String(50), nullable=False, unique = True)
    categoria = db.Column(db.String(50), nullable=False)

    def __repr__(self):
        return self.nombre_fondo

class datos_fondo(db.Model):
    id = db.Column(db.Integer, primary_key= True)
    serie = db.Column(db.String(10), nullable=False)
    nemotecnico = db.Column(db.String(20), nullable=False, unique=True)
    isin = db.Column(db.String(20), nullable=True, unique=True)
    ticker_bloomberg = db.Column(db.String(20), nullable=True)
    patrimonio = db.Column(db.Numeric(precision='20,4'), nullable=False, default=0)
    valor_cuota = db.Column(db.Numeric(precision='10,4'), nullable=False, default=0)
    fondo = db.Column(db.Integer, db.ForeignKey('fondo.id'), nullable=False)

    __table_args__ = (db.UniqueConstraint('nemotecnico','fondo', name='ui_df'),)

class tipos_archivo(db.Model):
    id = db.Column(db.Integer, primary_key= True)
    tipo = db.Column(db.String(30), nullable=False)
    nombre = db.Column(db.String(50), nullable=False)
    nom_doc = db.Column(db.String(30), nullable=False)

class noticia(db.Model):
    id = db.Column(db.Integer, primary_key= True)
    tipo = db.Column(db.String(20), nullable=False)
    titulo = db.Column(db.String(150), nullable=False)
    bajada = db.Column(db.String(255), nullable=True)
    contenido = db.Column(db.Text, nullable=True)
    fecha = db.Column(db.Date, nullable=False)
    imagen = db.Column(db.String(255), nullable=True)
    archivo = db.Column(db.String(255), nullable=True)

class formulario(db.Model):
    id = db.Column(db.Integer, primary_key= True)
    rut = db.Column(db.String(20), nullable=False, unique=True)
    nombre = db.Column(db.String(30), nullable=False)
    apellidoP = db.Column(db.String(30), nullable=False)
    apellidoM = db.Column(db.String(30), nullable=True)
    fecha_nac = db.Column(db.Date, nullable=True)
    nacionalidad = db.Column(db.String(30), nullable=True)
    estado = db.Column(db.String(20), nullable=True)
    direccion = db.Column(db.String(255), nullable=True)
    comuna = db.Column(db.String(50), nullable=True)
    ciudad = db.Column(db.String(50), nullable=True)
    pais = db.Column(db.String(50), nullable=True)
    telefono_f = db.Column(db.String(20), nullable=True)
    telefono_m = db.Column(db.String(20), nullable=True)
    email = db.Column(db.String(100), nullable=False, unique=True)
    profesion = db.Column(db.String(100), nullable=True)
    cargo = db.Column(db.String(100), nullable=True)
    empleador = db.Column(db.String(100), nullable=True)
    rut_emp = db.Column(db.String(20), nullable=True)
    banco = db.Column(db.String(100), nullable=True)
    cuenta = db.Column(db.String(20), nullable=True)
    fecha = db.Column(db.Date, nullable=False)
    eliminado = db.Column(db.Boolean,nullable= False)

class contacto(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    nombre = db.Column(db.String(20), unique=False, nullable=False)
    email = db.Column(db.String(120), unique=False, nullable=False)
    asunto = db.Column(db.String(255), nullable=False)
    mensaje = db.Column(db.Text, nullable=False)
    fecha = db.Column(db.Date, nullable=False)
    eliminado = db.Column(db.Boolean,nullable= False)

class webinar(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    nombre = db.Column(db.String(255), unique=False, nullable=False)
    resumen = db.Column(db.Text, nullable=True)
    descripcion = db.Column(db.Text, nullable=True)
    url = db.Column(db.String(255), unique=True, nullable=False)
    link_page = db.Column(db.String(255), unique=True, nullable=False)
    link_youtube = db.Column(db.String(255), nullable=False)
    chat = db.Column(db.Boolean,nullable= False)
    preguntas = db.Column(db.Boolean,nullable= True)
    dest_correos = db.Column(db.Text, unique=False, nullable=True)
    dest_nums = db.Column(db.Text, unique=False, nullable=True)
    privado = db.Column(db.Boolean,nullable= False)
    fecha = db.Column(db.DateTime, nullable = True)

class speaker_webinar(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    nombre = db.Column(db.String(50), unique=False, nullable=False)
    tipo = db.Column(db.Integer,nullable= False)
    foto = db.Column(db.String(50), nullable=True)
    #cargo = db.Column(db.String(50), unique=False, nullable=False)
    reseña = db.Column(db.Text, nullable=False)
    webinar = db.Column(db.Integer, db.ForeignKey('webinar.id'), nullable=False)


class clientes_webinar(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    clientes = db.Column(db.Text, nullable=True)
    clientes_publico = db.Column(db.Text, nullable=True)
    webinar = db.Column(db.Integer, db.ForeignKey('webinar.id'), nullable=False)
    ip_cliente =  db.Column(db.Text, nullable=True)

class usuarios_webinar(db.Model):
    rut =  db.Column(db.String(15),primary_key= True, unique=True, nullable=False)
    nombre =  db.Column(db.String(255), unique=False, nullable=False)
    ip =  db.Column(db.String(20), unique=False, nullable=False)
    fecha_ultimo_login = db.Column(db.Date, nullable=True)
    correo =  db.Column(db.String(100), unique=False, nullable=False)

class preguntas(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    pregunta =  db.Column(db.Text, unique=False, nullable=False)
    fecha_pregunta = db.Column(db.Date, nullable=True)
    usuario = db.Column(db.String(15), db.ForeignKey('usuarios_webinar.rut'), nullable=False)
    webinar = db.Column(db.Integer, db.ForeignKey('webinar.id'), nullable=False)