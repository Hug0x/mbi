from flask import Blueprint, render_template

errors = Blueprint('errors', __name__)


@errors.app_errorhandler(404)
def error_404(error):
    er = error
    return render_template('errores/error.html', er = er), 404


@errors.app_errorhandler(403)
def error_403(error):
    er = error
    return render_template('errores/error.html', er=er), 403


@errors.app_errorhandler(500)
def error_500(error):
    er = error
    return render_template('errores/error.html', er=er), 500
