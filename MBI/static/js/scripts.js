$(document).ready(function(){
    $(window).scroll(function(){
        var top = $(window).scrollTop();
        if(top > 10){
            $('nav.navbar').addClass('collapse-nav');
        }else{
            $('nav.navbar').removeClass('collapse-nav');
        }
    });
    $(window).trigger('scroll');

    $('.mbi-accordion').each(function(){
        var ref = $(this).data('ref');
        $(this).on('click', function(event){
            event.preventDefault();
            if(!$('#' + ref).hasClass('open-accordion-panel')){
                $('#' + ref).addClass('open-accordion-panel');
            }else{
                $('#' + ref).removeClass('open-accordion-panel');
            }
        });
    });

    var myEvents = [{
        date: '1998',
        content: '<strong>Fundada como agencia</strong> de Valores por dos de sus socios, Arturo Claro y Germán Guerrero.'
      },{
        date: '2001',
        content: '<strong>Se transforma en Corredora de Bolsa</strong> llegando a ser actualmente, la segunda corredora no bancaria en cuanto a patrimonio de Chile.'
      },{
        date: '2004',
        content: '<strong>Se incorporan como socios</strong> Jose Manuel Ugarte y Humberto Muñoz, dando origen a MBI Administradora General de Fondos, la que a fines de ese año colocó su primer fondo público de acciones chilenas, MBI Arbitrage Fondo de Inversión.'
      },
      {
        date: '2009',
        content: '<strong>Se incorporan dos nuevos socios</strong>, Jorge Del Puerto y Karin Küllmer, para formar MBI Inversiones y desarrollar el área de clientes privados, buscando nuevas alternativas de inversión según sus necesidades. De esta manera se amplía la oferta de productos, ajustándose siempre a los requerimientos de los clientes y oportunidades en el mercado.'
      },
      {
          date: '2010',
          content: '<strong>MBI Inversiones estructuró su división GPI</strong>, Gestión Patrimonial Integral, cuyo foco es asesorar a clientes de alto patrimonio en la Administración de Carteras y Family Office. Ese mismo año se creó el área de Inversiones Inmobiliarias, que a través de sus diversos fondos permite acceder a este atractivo mercado.'
      },
      {
        date: '2012',
        content: '<strong>Con el objetivo de ampliar</strong> las oportunidades de inversión para los clientes, se concreta una alianza comercial con la empresa peruana Andino Asset Management lanzando, un fondo de acciones orientado a este mercado.'
    },
    {
        date: '2013',
        content: '<strong>MBI Inversiones colocó</strong> su primer fondo de activos reales en el área de energía, que invierte en proyectos de generación eléctrica de mediana potencia, con el apoyo de gestores de reconocido prestigio y un grupo de aportantes altamente involucrados.'
    },
    {
        date: '2014',
        content: '<strong>Se crea el Área de Activos Alternativos</strong> que busca invertir en productos de deuda no tradicional proporcionando a sus clientes mayores alternativas de inversión.'
    },
    {
        date: '2017',
        content: '<strong>MBI abre al área Institucionales</strong>, <br>expandiendo así su foco de inversión.<br>Se alcanza un billón de dólares en activos manejados en diferentes fondos de inversión.<br>Al cierre de 2017, MBI Inversiones cuenta con 14 fondos de inversión públicos y 6 fondos privados.'
    }
  ];

    $('#mbi-timeline').roadmap(
        myEvents, 
        {
            eventsPerSlide: 3
        },
    );
});