from flask import render_template, request, Blueprint, flash, redirect, url_for, current_app, abort, send_file, jsonify
from flask_login import login_user, current_user, logout_user, login_required
from MBI.models import texto, User, fondo, datos_fondo, documento, tipos_archivo, noticia, contacto, formulario, webinar, speaker_webinar, clientes_webinar, usuarios_webinar, preguntas
from MBI.forms import formtexto, formLogin, formFP, formAccount, formArchivo, formAccountCorredora, formNoticia, formLegalesAGF, formContacto, formFondo, formDocFondo, formLoginWebinar, formWebinar, formSpeaker, formLoginWebinar2, formPreguntaWebinar
from MBI.funciones import enviar_mail, enviar_pregunta
from MBI import db, bcrypt, mail, socketio
import os, datetime, pyodbc, json, pyexcel, time
from datetime import timedelta
from flask_mail import Message
from flask_socketio import join_room, leave_room, send
main = Blueprint('main', __name__)

#chat
@socketio.on('incoming-msg')
def on_message(data):
    """Broadcast messages"""

    msg = data["msg"]
    username = data["username"]
    room = data["room"]

    ip_access = request.access_route[0]
    fecha = datetime.date.today()
    user = usuarios_webinar.query.filter_by(ip=ip_access).order_by(usuarios_webinar.fecha_ultimo_login.desc()).first()
    web = webinar.query.filter_by(link_page=room).first()
    preg = preguntas(pregunta=msg, fecha_pregunta=fecha, usuario=user.rut, webinar=web.id)
    db.session.add(preg)
    db.session.commit()
    # Set timestamp
    time_stamp = time.strftime('%b-%d %I:%M%p', time.localtime())
    send({"username": username, "msg": msg, "time_stamp": time_stamp}, room=room)
    msj = '<p>'+msg+'</p><br><p>Pregunta enviada por: '+user.nombre+' - '+user.correo+'</p>'
    enviar_pregunta('Pregunta Webinar de '+str(user.nombre),msj,web)


@socketio.on('join')
def on_join(data):
    """User joins a room"""

    username = data["username"]
    room = data["room"]
    join_room(room)

    # Broadcast that new user has joined
    send({"msg": username + " ha entrado al webinar"}, room=room)


@socketio.on('leave')
def on_leave(data):
    """User leaves a room"""

    username = data['username']
    room = data['room']
    leave_room(room)
    send({"msg": username + " ha dejado el webinar"}, room=room)

#-----------------------------------
@main.route("/layout")
def layout():
    return render_template('layout.html', title='layout')

@main.route("/test")
def test():
    webi = webinar.query.filter_by(id=31).first()
    ip = request.access_route[0]
    preg = formPreguntaWebinar()
    user = usuarios_webinar.query.filter_by(ip=ip).order_by(usuarios_webinar.fecha_ultimo_login.desc()).first()
    name = user.nombre#+' - '+user.correo
    return render_template('prueba.html', video= '', chat=webi.chat, speakers =[], mod = [], webi = webi, form = preg, rooms=webi.link_page, username = name)

@main.route("/webinar/<id_webinar>/chat")
def chat_webinar(id_webinar):
    webi = webinar.query.filter_by(id=id_webinar).first()
    return render_template('Admin/admin-chat-webinar.html', rooms=webi.link_page)

def leer_archivo(archivo):
    filename = archivo.filename
    extension = filename.split(".")[-1]
    content = archivo.read()
    sheet = pyexcel.get_array(file_type=extension, file_content=content, delimiter=';')
    del sheet[0]
    aux = {}
    for s in sheet:
        #aux = {}
        rut = str(s[0]).strip()
        rut = rut.replace('.', '')
        rut = rut.replace('-', '')
        rut = rut.lower()
        correo = s[1]
        aux[rut]={"correo": correo, "ip": '', "asistencia":''}
    return aux

@main.route("/api/pregunta/", methods=['POST'])
def pregunta():
    if request.method == 'POST':
        data = request.get_json()
        webi = data['webinar']
        tipo = data['tipo']
        msj = data['mensaje']
        ip_access = request.access_route[0]
        fecha = datetime.date.today()
        user = usuarios_webinar.query.filter_by(ip=ip_access, fecha_ultimo_login=fecha).first()
        web = webinar.query.filter_by(link_page=webi, privado=tipo).first()
        preg = preguntas(pregunta=msj, fecha_pregunta=fecha, usuario=user.rut, webinar=web.id)
        db.session.add(preg)
        db.session.commit()
        msj = '<p>'+msj+'</p><br><p>Pregunta enviada por: '+user.nombre+' - '+user.correo+'</p>'
        enviar_pregunta('Pregunta Webinar de '+str(user.nombre),msj,web)
        return jsonify({"status":"OK"}),200
    return jsonify({"status":"ERROR"}),400

@main.route("/admin/webinar/<id_webinar>/preguntas")
def ver_preguntas(id_webinar):
    pregs = preguntas.query.filter_by(webinar=id_webinar)
    pregunta = []
    for p in pregs:
        aux ={}
        aux['fecha'] = p.fecha_pregunta
        aux['pregunta'] = p.pregunta
        user = usuarios_webinar.query.filter_by(rut=p.usuario).first()
        aux['nombre'] = user.nombre
        aux['correo'] = user.correo
        pregunta.append(aux)
    return render_template('Admin/admin-ver-preguntas.html', data = pregunta, webinar = id_webinar)


@main.route("/webinar/<tipo>/<id_webinar>", methods=['GET', 'POST'])
def webinar_view(tipo, id_webinar):
    tip = int(tipo)
    webi = webinar.query.filter_by(link_page=id_webinar, privado=tip).first()
    ip = request.access_route[0]
    fecha = datetime.datetime.now() + timedelta(hours=1)
    if webi:
        print(fecha, webi.fecha)
        if fecha < webi.fecha:
            mensaje = 'El webinar aún no esta disponible, por favor ingresa nuevamente en la fecha y hora que corresponde.'
            return render_template('errores/error-webinar.html', title='Login', error = mensaje, error2='', tipo = 0)
        else:
            if tipo == '1' or tipo == 1:
                webi = webinar.query.filter_by(link_page=id_webinar, privado=1).first()
                if webi == None:
                    mensaje = 'Webinar no encontrado, revise la URL dada.'
                    return render_template('errores/error-webinar.html', title='Login', error = mensaje, error2='', tipo=0)
                else:
                    cli_webi = clientes_webinar.query.filter_by(webinar= webi.id).first()
                    mod = speaker_webinar.query.filter_by(webinar=webi.id, tipo = 1).first()
                    spk = speaker_webinar.query.filter_by(webinar=webi.id, tipo = 2)
                    aux = webi.link_youtube
                    if '=' in aux:
                        yt_video = aux.split('=')[-1]
                    else:
                        yt_video = aux.split('/')[-1]
                    if cli_webi != None:
                        ips = cli_webi.ip_cliente
                        ips = ips.split('-')
                        if ip in ips:
                            preg = formPreguntaWebinar()
                            user = usuarios_webinar.query.filter_by(ip=ip).order_by(usuarios_webinar.fecha_ultimo_login.desc()).first()
                            name = user.nombre#+' - '+user.correo
                            return render_template('webinar.html', video= yt_video, chat=webi.chat, speakers =spk, mod = mod, webi = webi, form = preg, rooms=webi.link_page, username = name)
                        else:
                            return redirect(url_for('.login_webinar',webinar_id=webi.link_page))
                    else:
                        mensaje = 'Existen inconvenientes con la URL dada.'
                        return render_template('errores/error-webinar.html', title='Login', error = mensaje, error2='', tipo=0)
            elif tipo == '0' or tipo == 0:
                webi = webinar.query.filter_by(link_page=id_webinar, privado=0).first()
                if webi == None:
                    mensaje = 'Webinar no encontrado, revise la URL dada.'
                    return render_template('errores/error-webinar.html', title='Login', error = mensaje, error2='', tipo=0)
                else:
                    cli_webi = clientes_webinar.query.filter_by(webinar= webi.id).first()
                    mod = speaker_webinar.query.filter_by(webinar=webi.id, tipo = 1).first()
                    spk = speaker_webinar.query.filter_by(webinar=webi.id, tipo = 2)
                    aux = webi.link_youtube
                    if '=' in aux:
                        yt_video = aux.split('=')[-1]
                    else:
                        yt_video = aux.split('/')[-1]
                    if cli_webi != None:
                        ips = cli_webi.ip_cliente
                        ips = ips.split('-')
                        if ip in ips:
                            preg = formPreguntaWebinar()
                            user = usuarios_webinar.query.filter_by(ip=ip).order_by(usuarios_webinar.fecha_ultimo_login.desc()).first()
                            name = user.nombre#+' - '+user.correo
                            return render_template('webinar.html', video= yt_video, chat=webi.chat, speakers =spk, mod = mod, webi = webi, form = preg, rooms=webi.link_page, username =name)
                        else:
                            return redirect(url_for('.login_webinar',webinar_id=webi.link_page))
                    else:
                        return redirect(url_for('.login_webinar',webinar_id=webi.link_page))
    else:
        mensaje = 'Url de webinar incorrecta.'
        return render_template('errores/error-webinar.html', title='Login', error = mensaje, error2='', tipo = 0)

@main.route("/webinar/login/<webinar_id>", methods=['GET', 'POST'])
def login_webinar(webinar_id):
    
    webi = webinar.query.filter_by(link_page=webinar_id).first()
    clients = clientes_webinar.query.filter_by(webinar=webi.id).first()
    ip_access = request.access_route[0]
    if webi:
        if clients:
            if ip_access in clients.ip_cliente:
                if webi.privado:
                    return redirect(url_for('.webinar_view', id_webinar = webi.link_page, tipo=1))
                else:
                    return redirect(url_for('.webinar_view', id_webinar = webi.link_page, tipo=0))
        if webi.privado == 1:
            form = formLoginWebinar()
            if form.validate_on_submit():
                mail = form.correo.data
                rut = form.password.data
                rut = rut.lower()
                lista = clients.clientes
                lista = json.loads(lista)
                try:
                    lista[str(rut)]
                    dato = lista[str(rut)]
                    if str(dato["correo"]).lower() == str(mail).lower():
                        dato["asistencia"] = 'si'
                        if dato["ip"] == '':
                            dato["ip"] = ip_access
                            clients.clientes = json.dumps(lista)
                            clients.ip_cliente = clients.ip_cliente+'-'+ip_access
                            db.session.commit()
                            return redirect(url_for('.webinar_view', id_webinar = webi.link_page, tipo= 1))
                        else:
                            mensaje = 'Error en las credenciales ingresadas, un usuario ya ha ingresado con estas.'
                            msj = '(No comparta sus credenciales o no podra acceder)'
                            return render_template('errores/error-webinar.html', title='Login', error = mensaje, error2=msj, tipo=webi.link_page)
                    else:
                        mensaje = 'Haz ingresado mal tus datos, por favor verifica tu rut y correo.'
                        #msj = '(Revise que su direccion de correo coincida con la direccion donde le llego la invitacion a este webinar)'
                        return render_template('errores/error-webinar.html', title='Login', error = mensaje, error2='', tipo=webi.link_page)
                except Exception:
                    mensaje = 'Haz ingresado mal tus datos, por favor verifica tu rut y correo.'
                    #msj = '(Su rut, sin puntos ni guion, incluyendo digito verificador)'
                    return render_template('errores/error-webinar.html', title='Login', error = mensaje, error2='', tipo=webi.link_page)
            return render_template('login-webinar.html', title='Login', form = form)
        else:
            form = formLoginWebinar2()
            if form.validate_on_submit():
                aux_rut = form.rut.data
                rut = str(aux_rut).replace('.','')
                rut = rut.replace('-','')
                rut = rut.lower()
                nombre = form.nombre.data
                apellido = form.apellido.data
                correo = form.correo.data
                user_aux = usuarios_webinar.query.filter_by(rut=rut).first()
                if user_aux:
                    user_aux.ip = ip_access
                    user_aux.fecha_ultimo_login = datetime.date.today()
                else:
                    user = usuarios_webinar(rut=rut,nombre =(nombre+' '+apellido), ip=ip_access, fecha_ultimo_login=datetime.date.today(), correo=correo )
                    db.session.add(user)
                db.session.commit()
                if clients != None:
                    cli_pub = json.loads(clients.clientes_publico)
                    aux = {}
                    aux[rut]={"correo": correo, "nombre": nombre+' '+apellido, "ip":ip_access}
                    try:
                        cli_pub[rut]
                        mensaje = 'Datos de usuario ya registrados, intente sus propios datos.'
                        #msj = '(Revise que su direccion de correo coincida con la direccion donde le llego la invitacion a este webinar)'
                        return render_template('errores/error-webinar.html', title='Login', error = mensaje, error2='', tipo=webi.link_page)
                    except Exception:
                        cli_pub.update(aux)
                        clients.clientes_publico = json.dumps(cli_pub)
                    clients.ip_cliente = clients.ip_cliente+'-'+ip_access
                    db.session.commit()
                    return redirect(url_for('.webinar_view', id_webinar = webi.link_page, tipo= 0))
                else:
                    aux = {}
                    aux[rut]={"correo": correo, "nombre": nombre+' '+apellido, "ip":ip_access}
                    cli = clientes_webinar(webinar=webi.id, clientes_publico=json.dumps(aux), ip_cliente=ip_access)
                    db.session.add(cli)
                    db.session.commit()
                    return redirect(url_for('.webinar_view', id_webinar = webi.link_page, tipo= 0))
            return render_template('login-webinar-publico.html', title='Login', form = form)
    else:
        mensaje = 'Url de webinar incorrecta.'
        return render_template('errores/error-webinar.html', title='Login', error = mensaje, error2='', tipo = 0)


@main.route("/admin/webinars/crear-webinar", methods=['GET', 'POST'])
def crear_webinar():
    form = formWebinar()
    if form.validate_on_submit():
        nombre = form.nombre.data
        resumen = form.resumen.data
        descripcion = form.descripcion.data
        link_yt = ''#form.link_youtube.data
        fecha = form.fecha.data
        hora = form.hora.data
        fecha_webinar = datetime.datetime.combine(fecha,hora)
        correos = form.correos.data
        telefonos = form.telefonos.data
        if form.chat.data:
            ch = 1
        else:
            ch = 0
        if form.privado.data:
            pv = 1
        else:
            pv = 0
        if form.preguntas.data:
            pg = 1
        else:
            pg = 0
        aux = nombre.split(' ')
        link_pg = ''
        for a in aux:
            link_pg += str(a[0])
        aux = str(fecha)[:10]
        aux = aux.replace('-','')
        link_pg = link_pg+aux
        num = webinar.query.filter(webinar.link_page.ilike('%'+link_pg+'%')).count()
        if num==0:
            url = 'www.mbi.cl/webinar/'+str(pv)+'/'+link_pg
        else:
            url = 'www.mbi.cl/webinar/'+str(pv)+'/'+link_pg+str(num)
            link_pg = link_pg+str(num)
        webi = webinar(nombre=nombre, link_youtube=link_yt, link_page =link_pg, url=url, privado=pv, chat=ch, preguntas=pg, fecha=fecha_webinar, resumen=resumen, descripcion=descripcion, dest_correos=correos, dest_nums=telefonos)
        db.session.add(webi)
        db.session.commit()
        if form.archivo.data:
            arch = form.archivo.data
            clientes = json.dumps(leer_archivo(arch))
            cli = clientes_webinar(webinar=webi.id, clientes=clientes, ip_cliente='')
            db.session.add(cli)
            db.session.commit()
        flash('El Webinar ha sido creado correctamente!', 'success')
        return redirect(url_for('.ver_webinars'))
    return render_template('Admin/admin-crear-webinar.html', form=form)

@main.route("/admin/webinars")
def ver_webinars():
    webinars = webinar.query.all()
    return render_template('Admin/admin-ver-webinar.html', data = webinars)

@main.route("/admin/webinars/edittar-<id_webinar>", methods=['GET', 'POST'])
def editar_webinar(id_webinar):
    form = formWebinar()
    form2 = formSpeaker()
    datos = webinar.query.filter_by(id=id_webinar).first()
    mod = speaker_webinar.query.filter_by(webinar=id_webinar, tipo = 1).first()
    spk = speaker_webinar.query.filter_by(webinar=id_webinar, tipo = 2).count()
    op_mod = 0
    if mod:
        op_mod = 1
    if spk > 3:
        op_mod = 2
    if mod and spk > 3 :
        op_mod = 3
    if form.validate_on_submit():
        datos.nombre = form.nombre.data
        #link_yt = form.link_youtube.data
        datos.fecha = datetime.datetime.combine(form.fecha.data,form.hora.data)
        datos.resumen = form.resumen.data
        datos.descripcion =form.descripcion.data
        datos.dest_correos = form.correos.data
        datos.dest_nums = form.telefonos.data
        if form.chat.data:
            datos.chat = 1
        else:
            datos.chat = 0
        if form.privado.data:
            datos.privado = 1
        else:
            datos.privado = 0
        if form.preguntas.data:
            datos.preguntas = 1
        else:
            datos.preguntas = 0
        db.session.commit()
        if form.archivo.data:
            arch = form.archivo.data
            clientes = json.dumps(leer_archivo(arch))
            cli_webi = clientes_webinar.query.filter_by(webinar = id_webinar).first()
            if cli_webi:
                aux = cli_webi.clientes
                for a in aux:
                    if a not in clientes:
                        clientes.update(a)
                cli_webi.clientes = clientes
            else:
                cli = clientes_webinar(webinar=webi.id, clientes=clientes, ip_cliente='')
                db.session.add(cli)
            db.session.commit()
        flash('El webinar ha sido actualizado correctamente!', 'success')
        return redirect(url_for('.ver_webinars'))

    elif request.method == 'GET':
        form.nombre.data = datos.nombre
        form.resumen.data = datos.resumen
        form.descripcion.data = datos.descripcion
        form.link_youtube.data = datos.link_youtube
        form.chat.data = datos.chat
        form.privado.data = datos.privado
        form.preguntas.data = datos.preguntas
        #aux = str(datos.fecha).split(' ')
        form.fecha.data = datos.fecha.date()
        form.hora.data = datos.fecha.time()
        form.correos.data = datos.dest_correos
        form.telefonos.data = datos.dest_nums

    elif form2.validate_on_submit():
        tipo = form2.tipo.data
        nombre = form2.nombre.data
        reseña = form2.reseña.data
        if form2.foto.data:
            arch = form2.foto.data
            nom_foto = arch.filename
            arch_path = os.path.join(current_app.root_path, 'static/uploads/webinars', nom_foto)
            arch.save(arch_path)
            speaker = speaker_webinar(nombre=nombre, tipo=tipo, reseña = reseña, webinar=id_webinar, foto=nom_foto)
        else:
            speaker = speaker_webinar(nombre=nombre, tipo=tipo, reseña = reseña, webinar=id_webinar)
        db.session.add(speaker)
        db.session.commit()
        if tipo == 1:
            flash('El Moderador ha sido agregador correctamente!', 'success')
        else:
            flash('El Speaker Invitado ha sido agregado correctamente!', 'success')
        return redirect(url_for('.editar_webinar', id_webinar=id_webinar))
    return render_template('Admin/admin-editar-webinar.html', form=form, form2 = form2, op_mod=op_mod)

@main.route("/admin/webinar/<id_webinar>/speakers")
def ver_speakers(id_webinar):
    speakers = speaker_webinar.query.filter_by(webinar=id_webinar)
    return render_template('Admin/admin-ver-speakers.html', data = speakers, webinar = id_webinar)

@main.route("/admin/webinar/<id_webinar>/speakers/editar-speaker-<id_speaker>", methods=['GET', 'POST'])
def editar_speaker(id_webinar,id_speaker):
    speaker = speaker_webinar.query.filter_by(id=id_speaker, webinar=id_webinar).first()
    form = formSpeaker()
    if form.validate_on_submit():
        speaker.nombre = form.nombre.data
        speaker.reseña = form.reseña.data
        if form.foto.data:
            if speaker.foto:
                os.remove(os.path.join(current_app.root_path, 'static/uploads/webinars', speaker.foto))
            arch = form.foto.data
            nom_foto = arch.filename
            arch_path = os.path.join(current_app.root_path, 'static/uploads/webinars', nom_foto)
            arch.save(arch_path)
            speaker.foto = nom_foto
        db.session.commit()
        flash('El Speaker ha sido actualizado correctamente!', 'success')
        return redirect(url_for('.ver_speakers', id_webinar=id_webinar))
    elif request.method == 'GET':
        form.nombre.data = speaker.nombre
        form.reseña.data = speaker.reseña

    return render_template('Admin/admin-editar-speaker.html', form = form, foto = speaker.foto, tipo=speaker.tipo)
#********************************** Rutas Web publica ESPAÑOL**********************************#
#********************************** Inicio **********************************#
@main.route("/")
@main.route("/Inicio")
def home():
    wealth = texto.query.filter_by(seccion='home', titulo='wealth-management', idioma='español').first()
    asset = texto.query.filter_by(seccion='home', titulo='asset-management', idioma='español').first()
    cdb = texto.query.filter_by(seccion='home', titulo='inicio-cdb', idioma='español').first()
    notis = noticia.query.filter_by(tipo='inicio').order_by(noticia.fecha.desc()).limit(3).all()
    return render_template('Version-ES/index.html', title = 'MBI Home Page', wealth = wealth, asset= asset, cdb=cdb, noticias = notis)

@main.route("/comunicacion-clientes")
def noticias_inicio():
    page = request.args.get('page', 1, type=int)
    noticias = noticia.query.filter_by(tipo='inicio')\
        .order_by(noticia.fecha.desc())\
        .paginate(page=page, per_page=6)
    return render_template('Version-ES/comunicacion-clientes-ver-todas.html', title='About US - Noticias', noticias = noticias)

@main.route("/comunicacion-clientes/ver-comunicaion<int:id>")
def ver_noticia_inicio(id):
    noti = noticia.query.filter_by(id=id).first()
    return render_template('Version-ES/ver-comunicacion.html', title='About US - Noticias', noticia = noti)


#********************************** Asset management **********************************#
@main.route("/asset-management")
def asset():
    servicios = texto.query.filter_by(seccion = 'asset-management', subseccion ='servicios', idioma='español').all()
    fondos = texto.query.filter_by(seccion = 'asset-management', subseccion ='fondos', idioma='español').first()
    acc_cl = fondo.query.filter_by(categoria='Acciones Chilenas').all()
    acc_int = fondo.query.filter_by(categoria='Acciones Internacionales').all()
    deu_nac = fondo.query.filter_by(categoria='Deuda Nacional').all()
    deu_int = fondo.query.filter_by(categoria='Deuda Internacional').all()
    otros = fondo.query.filter_by(categoria='Otros').all()

    return render_template('Version-ES/asset-mangement.html', title='Asset', data1 = servicios, data2=fondos, deu_nac=deu_nac, deu_int=deu_int, acc_cl=acc_cl, acc_int=acc_int, otros=otros)

@main.route("/asset-management/<ruta>")
def fondo_inversion(ruta):

    fnd = fondo.query.filter_by(ruta_web=ruta).first()
    txt = texto.query.filter_by(titulo=fnd.nemo, idioma='español').first()
    
    form = formDocFondo()
    facsheets = []
    facts = documento.query.filter_by(fondo=fnd.id, tipo=1, visible=1).order_by(documento.nombre.asc())
    if facts:
        facsheets.append(("#", "Seleccione..."))
        for f in facts:
            facsheets.append((f.link, f.nombre))

    eeffs = []
    efs = documento.query.filter_by(fondo=fnd.id, tipo=2, visible=1).order_by(documento.nombre.desc())
    if efs:
        eeffs.append(("#", "Seleccione..."))
        for e in efs:
            eeffs.append((e.link, e.nombre))

    folletos = []
    foll = documento.query.filter_by(fondo=fnd.id, tipo=3, visible=1).order_by(documento.nombre.asc())
    if foll:
        folletos.append(("#", "Seleccione..."))
        for f in foll:
            folletos.append((f.link, f.nombre))

    infos = []
    inf = documento.query.filter_by(fondo=fnd.id, tipo=4, visible=1).order_by(documento.nombre.asc())
    if inf:
        infos.append(("#", "Seleccione..."))
        for i in inf:
            infos.append((i.link, i.nombre))

    reglam = documento.query.filter_by(fondo=fnd.id, tipo=5, visible=1).order_by(documento.id.desc()).first()

    form.factsheet.choices=[(f) for f in facsheets]
    form.eeff.choices=[(e) for e in eeffs]
    form.folleto.choices=[(f) for f in folletos]
    form.informacion.choices=[(i) for i in infos]

    conn = pyodbc.connect('Driver={ODBC Driver 17 for SQL Server};'
                      'Server=201.238.196.92,1434;'
                      'Database=BDValorCuotaMBI;'
                      'UID=usrBindex;'
                      'PWD=usrbindex202007.')

    with conn:
        csr = conn.cursor()
        csr.execute("SELECT * FROM WEB_ValoresCuota WHERE Codigo_Fdo = '"+fnd.nemo+"' ORDER BY Nombre_Serie ASC")
        data = csr.fetchall()
        csr.execute("SELECT TOP 1 Fecha FROM WEB_ValoresCuota WHERE Codigo_Fdo = '"+fnd.nemo+"'")
        fecha = csr.fetchall()
        try:
            fecha = fecha[0].Fecha
        except Exception:
            fecha = ''

    return render_template('Version-ES/info-fondos.html', title='Arbitrage', fecha=fecha, data=data, fondo=fnd, texto=txt, form=form, reglamento=reglam)


#********************************** Wealth management **********************************#
@main.route("/wealth-management")
def wealth():
    ns = texto.query.filter_by(seccion='wealth management', titulo='nuestros servicios', idioma='español').first()
    np = texto.query.filter_by(seccion='wealth management', titulo='nuestros productos', idioma='español').first()
    return render_template('Version-ES/wealth-mangement.html', title='Wealth', ns=ns, np=np)

#********************************** Corredores de bolsa **********************************#
@main.route("/corredores")
def corredores():
    textos = texto.query.filter_by(seccion='corredores de bolsa', idioma='español')
    noti = noticia.query.filter_by(tipo="bolsa").order_by(noticia.fecha.desc()).limit(3)
    return render_template('Version-ES/corredores-de-bolsa.html', title='Corredores', data=textos, noticias = noti)

@main.route("/corredores/publicaciones/ver-publicacion<int:id>")
def cdb_noticia(id):
    noti = noticia.query.filter_by(id=id).first()
    return render_template('Version-ES/corredores-de-bolsa-noticias.html', title='About US - Noticias', noticia = noti)

@main.route("/corredores/publicaciones")
def noticias_cdb():
    page = request.args.get('page', 1, type=int)
    noticias = noticia.query.filter_by(tipo='bolsa')\
        .order_by(noticia.fecha.desc())\
        .paginate(page=page, per_page=6)
    return render_template('Version-ES/corredores-de-bolsa-ver-todas.html', title='About US - Noticias', noticias = noticias)

@main.route("/register-corredora", methods=['GET', 'POST'])
def registerC():
    form = formAccountCorredora()
    opciones =[("--", "--"),("soltero","Soltero"),("casado","Casado / Conviviente Civil"),("divorciado","Divorciado"),("viudo","Viudo")]
    form.estado.choices = [(op) for op in opciones]
    if form.validate_on_submit():
        rut = form.rut.data
        nom = form.nombre.data
        ap = form.apellidoP.data
        am = form.apellidoM.data
        f_nac = form.fecha_nac.data
        nac = form.nacionalidad.data
        est = form.estado.data
        direc = form.direccion.data
        comuna = form.comuna.data
        ciudad = form.ciudad.data
        pais = form.pais.data
        tel_f = form.telefono_f.data
        tel_m = form.telefono_m.data
        email = form.email.data
        prof = form.profesion.data
        cargo = form.cargo.data
        emp = form.empleador.data
        rut_emp = form.rut_emp.data
        banco = form.banco.data
        cuenta = form.cuenta.data
        fecha = datetime.date.today()
        formu = formulario(rut=rut, nombre=nom, apellidoP=ap, apellidoM=am, fecha_nac=f_nac, nacionalidad=nac, estado=est, direccion=direc, comuna=comuna, ciudad=ciudad, pais=pais, telefono_f=tel_f, telefono_m=tel_m, email=email, profesion=prof, cargo=cargo, empleador=emp, rut_emp=rut_emp, banco=banco, cuenta=cuenta, fecha=fecha, eliminado=0)
        db.session.add(formu)
        db.session.commit()

        titulo = 'Formulario Corredora de: '+str(nom)+' '+str(ap)
        cuerpo = '<p>Se ha completado un nuevo formulario de Corredores de Bolsa</p> <p><strong>    -Nombre:</strong> '+str(nom)+' '+str(ap)+' '+str(am)+'</p><p><strong>    -Email:</strong> '+str(email)+'</p><p><strong>    -Telefono:</strong> '+str(tel_m)+'</p> <p>Para revisar los datos completos revisar formulario completo en plataforma de administracion o contacte al usuario con los datos de este correo</p>'

        enviar_mail(titulo, cuerpo , 'contacto@mbi.cl')

        flash('Se ha enviado tu formulario', 'success')
        return redirect(url_for('.registerC'))
    return render_template('Version-ES/register-corredora.html', title='Registro', form=form)

#********************************** Activos Alternativos **********************************#
@main.route("/activos-alternativos")
def activos():
    textos = texto.query.filter_by(seccion='activos alternativos', idioma='español')
    return render_template('Version-ES/activos-alternativos.html', title='Activos', data=textos)

#********************************** Quienes Somos **********************************#
### Rutas pagina principal ###
@main.route("/quienes-somos")
def about():
    textos = texto.query.filter_by(seccion='quienes somos', subseccion=None, idioma='español')
    return render_template('Version-ES/quienes-somos.html', title='About US', data=textos)

@main.route("/quienes-somos/hitos")
def hitos():
    textos = texto.query.filter_by(seccion='hitos', idioma='español').order_by('titulo')
    return render_template('Version-ES/hitos.html', title='About US - Hitos', data=textos)

@main.route("/quienes-somos/socios")
def socios():
    textos = texto.query.filter_by(seccion='quienes somos', subseccion='socios', idioma='español')
    return render_template('Version-ES/socios.html', title='About US - Socios', data=textos)

@main.route("/quienes-somos/MBI-en-la-prensa")
def noticias():
    page = request.args.get('page', 1, type=int)
    noticias = noticia.query.filter_by(tipo='mbi-prensa')\
        .order_by(noticia.fecha.desc())\
        .paginate(page=page, per_page=6)
    return render_template('Version-ES/mbienlaprensa.html', title='About US - Noticias', noticias = noticias)

@main.route("/quienes-somos/MBI-en-la-prensa/ver-noticia<int:id>")
def ver_noticia(id):
    noti = noticia.query.filter_by(id=id).first()
    return render_template('Version-ES/mbienlaprensa-noticias.html', title='About US - Noticias', noticia = noti)

#********************************** Otros **********************************#
@main.route("/legales-agf")
def legalesagf():
    textos = texto.query.filter_by(seccion='legales', idioma='español').first()
    form = formLegalesAGF()
    
    memoria = []
    aux = tipos_archivo.query.filter_by(tipo = 'legal-agf', nombre='Memoria').first()
    mems = documento.query.filter_by(tipo=aux.id, visible=1).order_by(documento.nombre.desc())
    if mems:
        memoria.append(("#", "Seleccione..."))
        for m in mems:
            memoria.append((m.link, m.nombre))

    eeffs = []
    aux = tipos_archivo.query.filter_by(tipo = 'legal-agf', nombre='Estados Financieros').first()
    eeff = documento.query.filter_by(tipo=aux.id, visible=1).order_by(documento.nombre.desc())
    if eeff:
        eeffs.append(("#", "Seleccione..."))
        for e in eeff:
            eeffs.append((e.link, e.nombre))

    manual = []
    aux = tipos_archivo.query.filter_by(tipo = 'legal-agf', nombre='Manuales').first()
    manuals = documento.query.filter_by(tipo=aux.id, visible=1).order_by(documento.nombre.desc())
    if manuals:
        manual.append(("#", "Seleccione..."))
        for m in manuals:
            manual.append((m.link, m.nombre))

    otro = []
    aux = tipos_archivo.query.filter_by(tipo = 'legal-agf', nombre='Otros').first()
    otros = documento.query.filter_by(tipo=aux.id, visible=1).order_by(documento.nombre.desc())
    if otros:
        otro.append(("#", "Seleccione..."))
        for o in otros:
            otro.append((o.link, o.nombre))

    form.memorias.choices=[(f) for f in memoria]
    form.eeff.choices=[(e) for e in eeffs]
    form.manuales.choices=[(f) for f in manual]
    form.otros.choices=[(i) for i in otro]
    return render_template('Version-ES/legales-agf.html', title='layout', data = textos, form = form)

@main.route("/legales-inv")
def legalesinv():
    textos = texto.query.filter_by(seccion='legales-inv', idioma='español').first()
    form = formLegalesAGF()

    eeffs = []
    aux = tipos_archivo.query.filter_by(tipo = 'legal-inv', nombre='Estados Financieros').first()
    eeff = documento.query.filter_by(tipo=aux.id, visible=1).order_by(documento.nombre.desc())
    if eeff:
        eeffs.append(("#", "Seleccione..."))
        for e in eeff:
            eeffs.append((e.link, e.nombre))

    form.eeff.choices=[(e) for e in eeffs]

    return render_template('Version-ES/legales-inv.html', title='layout', data = textos, form = form)

@main.route("/legales-cdb")
def legalescdb():
    textos = texto.query.filter_by(seccion='legales-cdb', idioma='español')
    form = formLegalesAGF()
    
    memoria = []
    aux = tipos_archivo.query.filter_by(tipo = 'legal-cdb', nombre='Informes de Auditoria').first()
    mems = documento.query.filter_by(tipo=aux.id, visible=1).order_by(documento.nombre.desc())
    if mems:
        memoria.append(("#", "Seleccione..."))
        for m in mems:
            memoria.append((m.link, m.nombre))

    eeffs = []
    aux = tipos_archivo.query.filter_by(tipo = 'legal-cdb', nombre='Indices de Liquidez y Solvencia').first()
    eeff = documento.query.filter_by(tipo=aux.id, visible=1).order_by(documento.nombre.desc())
    if eeff:
        eeffs.append(("#", "Seleccione..."))
        for e in eeff:
            eeffs.append((e.link, e.nombre))

    manual = []
    aux = tipos_archivo.query.filter_by(tipo = 'legal-cdb', nombre='Estados Financieros').first()
    manuals = documento.query.filter_by(tipo=aux.id, visible=1).order_by(documento.nombre.desc())
    if manuals:
        manual.append(("#", "Seleccione..."))
        for m in manuals:
            manual.append((m.link, m.nombre))

    otro = []
    aux = tipos_archivo.query.filter_by(tipo = 'legal-cdb', nombre='Politicas, Procedimientos y Manuales').first()
    otros = documento.query.filter_by(tipo=aux.id, visible=1).order_by(documento.nombre.desc())
    if otros:
        otro.append(("#", "Seleccione..."))
        for o in otros:
            otro.append((o.link, o.nombre))

    doc = documento.query.filter_by(nombre ='Cuentas Mandantes MBI CB', visible=1).first()

    form.memorias.choices=[(f) for f in memoria]
    form.eeff.choices=[(e) for e in eeffs]
    form.manuales.choices=[(f) for f in manual]
    form.otros.choices=[(i) for i in otro]

    return render_template('Version-ES/legales-cdb.html', title='layout', data = textos, form = form, doc=doc)

@main.route("/contacto", methods=['GET', 'POST'])
def pag_contacto():
    form = formContacto()
    if form.validate_on_submit():
        con = contacto(nombre=form.nombre.data, email=form.email.data, asunto=form.asunto.data, mensaje=form.mensaje.data, fecha=datetime.date.today(), eliminado=0)
        db.session.add(con)
        db.session.commit()
        #subj = 'Asunto: '+str(form.asunto.data)
        #msg = Message( subj,
        #          sender='noreply@demo.com',
        #          recipients=['hgo.saavedra@gmail.com'])
        #msg.body = 'Datos:\n\tNombre: '+str(form.nombre.data)+'\n\tEmail: '+str(form.email.data)+'\n\nMensaje: '+str(form.mensaje.data)
        #mail.send(msg)

        titulo = 'Formulario de contacto de: '+str(form.nombre.data)
        cuerpo = '<h2><strong>Datos:</strong></h2> <p><strong>    -Nombre:</strong> '+str(form.nombre.data)+'</p><p><strong>    -Email:</strong> '+str(form.email.data)+'</p><p><strong>    -Mensaje:</strong> '+str(form.mensaje.data)+'</p>'

        enviar_mail(titulo, cuerpo , 'contacto@mbi.cl')

        flash('Se ha enviado tu mensaje!', 'success')
        return redirect(url_for('.pag_contacto'))
    return render_template('Version-ES/contacto.html', title='Contacto', form=form)

#********************************** Rutas Web publica INGLES**********************************#
#********************************** Inicio **********************************#
@main.route("/EN-en/")
@main.route("/EN-en/Inicio")
def home_EN():
    wealth = texto.query.filter_by(seccion='home', titulo='wealth-management', idioma='ingles').first()
    asset = texto.query.filter_by(seccion='home', titulo='asset-management', idioma='ingles').first()
    cdb = texto.query.filter_by(seccion='home', titulo='inicio-cdb', idioma='ingles').first()
    return render_template('Version-EN/index-EN.html', title = 'MBI Home Page', wealth = wealth, asset= asset, cdb=cdb)


#********************************** Asset management **********************************#
@main.route("/EN-en/asset-management")
def asset_EN():
    servicios = texto.query.filter_by(seccion = 'asset-management', subseccion ='servicios', idioma='ingles').all()
    fondos = texto.query.filter_by(seccion = 'asset-management', subseccion ='fondos', idioma='ingles').first()
    acc_cl = fondo.query.filter_by(categoria='Acciones Chilenas').all()
    acc_int = fondo.query.filter_by(categoria='Acciones Internacionales').all()
    deu_nac = fondo.query.filter_by(categoria='Deuda Nacional').all()
    deu_int = fondo.query.filter_by(categoria='Deuda Internacional').all()
    otros = fondo.query.filter_by(categoria='Otros').all()

    return render_template('Version-EN/asset-mangement-EN.html', title='Asset', data1 = servicios, data2=fondos, deu_nac=deu_nac, deu_int=deu_int, acc_cl=acc_cl, acc_int=acc_int, otros=otros)

@main.route("/EN-en/asset-management/<ruta>")
def fondo_inversion_EN(ruta):

    fnd = fondo.query.filter_by(ruta_web=ruta).first()
    txt = texto.query.filter_by(titulo=fnd.nemo, idioma='español').first()
    
    form = formDocFondo()
    facsheets = []
    facts = documento.query.filter_by(fondo=fnd.id, tipo=1, visible=1).order_by(documento.nombre.asc())
    if facts:
        facsheets.append(("#", "Seleccione..."))
        for f in facts:
            facsheets.append((f.link, f.nombre))

    eeffs = []
    efs = documento.query.filter_by(fondo=fnd.id, tipo=2, visible=1).order_by(documento.nombre.desc())
    if efs:
        eeffs.append(("#", "Seleccione..."))
        for e in efs:
            eeffs.append((e.link, e.nombre))

    folletos = []
    foll = documento.query.filter_by(fondo=fnd.id, tipo=3, visible=1).order_by(documento.nombre.asc())
    if foll:
        folletos.append(("#", "Seleccione..."))
        for f in foll:
            folletos.append((f.link, f.nombre))

    infos = []
    inf = documento.query.filter_by(fondo=fnd.id, tipo=4, visible=1).order_by(documento.nombre.asc())
    if inf:
        infos.append(("#", "Seleccione..."))
        for i in inf:
            infos.append((i.link, i.nombre))

    reglam = documento.query.filter_by(fondo=fnd.id, tipo=5, visible=1).order_by(documento.id.desc()).first()

    form.factsheet.choices=[(f) for f in facsheets]
    form.eeff.choices=[(e) for e in eeffs]
    form.folleto.choices=[(f) for f in folletos]
    form.informacion.choices=[(i) for i in infos]

    conn = pyodbc.connect('Driver={ODBC Driver 17 for SQL Server};'
                      'Server=201.238.196.92,1434;'
                      'Database=BDValorCuotaMBI;'
                      'UID=usrBindex;'
                      'PWD=usrbindex202007.')

    with conn:
        csr = conn.cursor()
        csr.execute("SELECT * FROM WEB_ValoresCuota WHERE Codigo_Fdo = '"+fnd.nemo+"' ORDER BY Nombre_Serie ASC")
        data = csr.fetchall()
        csr.execute("SELECT TOP 1 Fecha FROM WEB_ValoresCuota WHERE Codigo_Fdo = '"+fnd.nemo+"'")
        fecha = csr.fetchall()
        try:
            fecha = fecha[0].Fecha
        except Exception:
            fecha = ''

    return render_template('Version-EN/info-fondos-EN.html', title='Arbitrage', fecha=fecha, data=data, fondo=fnd, texto=txt, form=form, reglamento=reglam)


#********************************** Wealth management **********************************#
@main.route("/EN-en/wealth-management")
def wealth_EN():
    np = texto.query.filter_by(seccion='wealth management', titulo='nuestros productos', idioma='ingles').first()
    return render_template('Version-EN/wealth-mangement-EN.html', title='Wealth', np=np)

#********************************** Corredores de bolsa **********************************#
@main.route("/EN-en/corredores")
def corredores_EN():
    textos = texto.query.filter_by(seccion='corredores de bolsa', idioma='ingles')
    noti = noticia.query.filter_by(tipo="bolsa").order_by(noticia.fecha.desc()).limit(3)
    return render_template('Version-EN/corredores-de-bolsa-EN.html', title='Corredores', data=textos, noticias = noti)

@main.route("/EN-en/register-corredora", methods=['GET', 'POST'])
def registerC_EN():
    form = formAccountCorredora()
    opciones =[("--", "--"),("soltero","Single"),("casado","Married"),("divorciado","Divorced"),("viudo","Widoved")]
    form.estado.choices = [(op) for op in opciones]
    if form.validate_on_submit():
        rut = form.rut.data
        nom = form.nombre.data
        ap = form.apellidoP.data
        am = form.apellidoM.data
        f_nac = form.fecha_nac.data
        nac = form.nacionalidad.data
        est = form.estado.data
        direc = form.direccion.data
        comuna = form.comuna.data
        ciudad = form.ciudad.data
        pais = form.pais.data
        tel_f = form.telefono_f.data
        tel_m = form.telefono_m.data
        email = form.email.data
        prof = form.profesion.data
        cargo = form.cargo.data
        emp = form.empleador.data
        rut_emp = form.rut_emp.data
        banco = form.banco.data
        cuenta = form.cuenta.data
        fecha = datetime.date.today()
        formu = formulario(rut=rut, nombre=nom, apellidoP=ap, apellidoM=am, fecha_nac=f_nac, nacionalidad=nac, estado=est, direccion=direc, comuna=comuna, ciudad=ciudad, pais=pais, telefono_f=tel_f, telefono_m=tel_m, email=email, profesion=prof, cargo=cargo, empleador=emp, rut_emp=rut_emp, banco=banco, cuenta=cuenta, fecha=fecha, eliminado=0)
        db.session.add(formu)
        db.session.commit()
        
        titulo = 'Formulario Corredora de: '+str(nom)+' '+str(ap)
        cuerpo = '<p>Se ha completado un nuevo formulario de Corredores de Bolsa</p> <p><strong>    -Nombre:</strong> '+str(nom)+' '+str(ap)+' '+str(am)+'</p><p><strong>    -Email:</strong> '+str(email)+'</p><p><strong>    -Telefono:</strong> '+str(tel_m)+'</p> <p>Para revisar los datos completos revisar formulario completo en plataforma de administracion o contacte al usuario con los datos de este correo</p>'

        enviar_mail(titulo, cuerpo , 'contacto@mbi.cl')

        flash('Se ha enviado tu formulario', 'success')
        return redirect(url_for('.registerC'))
    return render_template('Version-EN/register-corredora-EN.html', title='Registro', form=form)

#********************************** Activos Alternativos **********************************#
@main.route("/EN-en/activos-alternativos")
def activos_EN():
    textos = texto.query.filter_by(seccion='activos alternativos', idioma='ingles')
    return render_template('Version-EN/activos-alternativos-EN.html', title='Activos', data=textos)

#********************************** Quienes Somos **********************************#
### Rutas pagina principal ###
@main.route("/EN-en/quienes-somos")
def about_EN():
    textos = texto.query.filter_by(seccion='quienes somos', subseccion=None, idioma='ingles')
    return render_template('Version-EN/quienes-somos-EN.html', title='About US', data=textos)

@main.route("/EN-en/quienes-somos/socios")
def socios_EN():
    textos = texto.query.filter_by(seccion='quienes somos', subseccion='socios', idioma='ingles')
    return render_template('Version-EN/socios-EN.html', title='About US - Socios', data=textos)

#********************************** Otros **********************************#
@main.route("/EN-en/legales-agf")
def legalesagf_EN():
    textos = texto.query.filter_by(seccion='legales', idioma='ingles').first()
    form = formLegalesAGF()
    
    memoria = []
    aux = tipos_archivo.query.filter_by(tipo = 'legal-agf', nombre='Memoria').first()
    mems = documento.query.filter_by(tipo=aux.id, visible=1).order_by(documento.nombre.desc())
    if mems:
        memoria.append(("#", "Seleccione..."))
        for m in mems:
            memoria.append((m.link, m.nombre))

    eeffs = []
    aux = tipos_archivo.query.filter_by(tipo = 'legal-agf', nombre='Estados Financieros').first()
    eeff = documento.query.filter_by(tipo=aux.id, visible=1).order_by(documento.nombre.desc())
    if eeff:
        eeffs.append(("#", "Seleccione..."))
        for e in eeff:
            eeffs.append((e.link, e.nombre))

    manual = []
    aux = tipos_archivo.query.filter_by(tipo = 'legal-agf', nombre='Manuales').first()
    manuals = documento.query.filter_by(tipo=aux.id, visible=1).order_by(documento.nombre.desc())
    if manuals:
        manual.append(("#", "Seleccione..."))
        for m in manuals:
            manual.append((m.link, m.nombre))

    otro = []
    aux = tipos_archivo.query.filter_by(tipo = 'legal-agf', nombre='Otros').first()
    otros = documento.query.filter_by(tipo=aux.id, visible=1).order_by(documento.nombre.desc())
    if otros:
        otro.append(("#", "Seleccione..."))
        for o in otros:
            otro.append((o.link, o.nombre))

    form.memorias.choices=[(f) for f in memoria]
    form.eeff.choices=[(e) for e in eeffs]
    form.manuales.choices=[(f) for f in manual]
    form.otros.choices=[(i) for i in otro]
    return render_template('Version-EN/legales-agf-EN.html', title='layout', data = textos, form = form)

@main.route("/EN-en/legales-inv")
def legalesinv_EN():
    textos = texto.query.filter_by(seccion='legales-inv', idioma='ingles').first()
    form = formLegalesAGF()

    eeffs = []
    aux = tipos_archivo.query.filter_by(tipo = 'legal-inv', nombre='Estados Financieros').first()
    eeff = documento.query.filter_by(tipo=aux.id, visible=1).order_by(documento.nombre.desc())
    if eeff:
        eeffs.append(("#", "Seleccione..."))
        for e in eeff:
            eeffs.append((e.link, e.nombre))

    form.eeff.choices=[(e) for e in eeffs]

    return render_template('Version-EN/legales-inv-EN.html', title='layout', data = textos, form = form)

@main.route("/EN-en/legales-cdb")
def legalescdb_EN():
    textos = texto.query.filter_by(seccion='legales-cdb', idioma='ingles')
    form = formLegalesAGF()
    
    memoria = []
    aux = tipos_archivo.query.filter_by(tipo = 'legal-cdb', nombre='Informes de Auditoria').first()
    mems = documento.query.filter_by(tipo=aux.id, visible=1).order_by(documento.nombre.desc())
    if mems:
        memoria.append(("#", "Seleccione..."))
        for m in mems:
            memoria.append((m.link, m.nombre))

    eeffs = []
    aux = tipos_archivo.query.filter_by(tipo = 'legal-cdb', nombre='Indices de Liquidez y Solvencia').first()
    eeff = documento.query.filter_by(tipo=aux.id, visible=1).order_by(documento.nombre.desc())
    if eeff:
        eeffs.append(("#", "Seleccione..."))
        for e in eeff:
            eeffs.append((e.link, e.nombre))

    manual = []
    aux = tipos_archivo.query.filter_by(tipo = 'legal-cdb', nombre='Estados Financieros').first()
    manuals = documento.query.filter_by(tipo=aux.id, visible=1).order_by(documento.nombre.desc())
    if manuals:
        manual.append(("#", "Seleccione..."))
        for m in manuals:
            manual.append((m.link, m.nombre))

    otro = []
    aux = tipos_archivo.query.filter_by(tipo = 'legal-cdb', nombre='Politicas, Procedimientos y Manuales').first()
    otros = documento.query.filter_by(tipo=aux.id, visible=1).order_by(documento.nombre.desc())
    if otros:
        otro.append(("#", "Seleccione..."))
        for o in otros:
            otro.append((o.link, o.nombre))

    doc = documento.query.filter_by(nombre ='Cuentas Mandantes MBI CB', visible=1).first()

    form.memorias.choices=[(f) for f in memoria]
    form.eeff.choices=[(e) for e in eeffs]
    form.manuales.choices=[(f) for f in manual]
    form.otros.choices=[(i) for i in otro]

    return render_template('Version-EN/legales-cdb-EN.html', title='layout', data = textos, form = form, doc=doc)

@main.route("/EN-en/contacto", methods=['GET', 'POST'])
def pag_contacto_EN():
    form = formContacto()
    if form.validate_on_submit():
        con = contacto(nombre=form.nombre.data, email=form.email.data, asunto=form.asunto.data, mensaje=form.mensaje.data, fecha=datetime.date.today(), eliminado=0)
        db.session.add(con)
        db.session.commit()
        #subj = 'Asunto: '+str(form.asunto.data)
        #msg = Message( subj,
        #          sender='noreply@demo.com',
        #          recipients=['hgo.saavedra@gmail.com'])
        #msg.body = 'Datos:\n\tNombre: '+str(form.nombre.data)+'\n\tEmail: '+str(form.email.data)+'\n\nMensaje: '+str(form.mensaje.data)
        #mail.send(msg)

        titulo = 'Formulario de contacto de: '+str(form.nombre.data)
        cuerpo = '<h2><strong>Datos:</strong></h2> <p><strong>    -Nombre:</strong> '+str(form.nombre.data)+'</p><p><strong>    -Email:</strong> '+str(form.email.data)+'</p><p><strong>    -Mensaje:</strong> '+str(form.mensaje.data)+'</p>'

        enviar_mail(titulo, cuerpo , 'contacto@mbi.cl')

        flash('Se ha enviado tu mensaje!', 'success')
        return redirect(url_for('.pag_contacto'))
    return render_template('Version-EN/contacto-EN.html', title='Contacto', form=form)


#********************************** Rutas Admin **********************************#
#********************************** Login **********************************#
@main.route("/login", methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('.home'))
    form = formLogin()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.usuario.data).first()
        if user and bcrypt.check_password_hash(user.password, form.contraseña.data):
            login_user(user, remember=form.recordar.data)
            return redirect(url_for('.home'))
        else:
            flash('No se ha podido autentificar, por favor verifique usuario y contraseña', 'danger')
    return render_template('Version-ES/login.html', title='Login', form = form)

@main.route("/logout")
def logout():
    logout_user()
    return redirect(url_for('.home'))

@main.route("/reset-password")
def reset():
    form = formFP()
    return render_template('Admin/forgot-password.html', title='Reestablecer Contraseña', form=form)

@main.route("/register", methods=['GET', 'POST'])
def register():
    form = formAccount()
    if form.validate_on_submit():
        hashed_password = bcrypt.generate_password_hash(form.contraseña.data).decode('utf-8')
        user = User(nombre=form.nombre.data, apellido=form.apellido.data, username=form.username.data, email=form.email.data, password=hashed_password, tipo_user=0)
        db.session.add(user)
        db.session.commit()
        flash('Tu cuenta ha sido creada con exito! Ya puedes iniciar sesion', 'success')
        return redirect(url_for('.login'))
    return render_template('Admin/register.html', title='Registro', form=form)

@main.route("/admin")
def admin():
    return render_template('Admin/admin-index.html', title='Administracion')

#********************************** Informacion y datos **********************************#
@main.route("/admin/textos")
def tablas():
    #datos = texto.query.filter_by(texto.seccion != "hitos")
    home = texto.query.filter_by(seccion = 'home', idioma='español')
    asset = texto.query.filter_by(seccion = 'asset-management', idioma='español')
    wealth = texto.query.filter_by(seccion = 'wealth management', idioma='español')
    cdb = texto.query.filter_by(seccion = 'corredores de bolsa', idioma='español')
    ac_alt = texto.query.filter_by(seccion = 'activos alternativos', idioma='español')
    qs = texto.query.filter_by(seccion = 'quienes somos', idioma='español').filter(texto.seccion != 'hitos')
    legales = texto.query.filter(texto.seccion.contains('legales'))
    txt_ingles = texto.query.filter_by(idioma='ingles')

    return render_template('Admin/admin-tables.html', title='Administracion - Textos', home = home, asset=asset, wealth=wealth, cdb=cdb, ac_alt=ac_alt, qs=qs, legales=legales, txt_EN=txt_ingles)

@main.route("/admin/textos/update<int:id_txt>", methods=['GET', 'POST'])
def update_texto(id_txt):
    datos = texto.query.filter_by(id=id_txt).first()
    form = formtexto()
    if form.validate_on_submit():
        datos.titulo = form.titulo.data
        datos.seccion = form.seccion.data
        if form.subseccion.data:
            datos.subseccion = form.subseccion.data
        else:
            datos.subseccion = None
        datos.contenido = form.contenido.data
        db.session.commit()
        flash('El texto ha sido actualizado correctamente!', 'success')
        return redirect(url_for('.tablas'))
    elif request.method == 'GET':
        form.titulo.data = datos.titulo
        form.seccion.data = datos.seccion
        form.subseccion.data = datos.subseccion
        form.contenido.data = datos.contenido
    return render_template('Admin/admin-tables-update.html', title='Administracion - Editar', form = form)

@main.route("/admin/hitos")
def txt_hitos():
    datos = texto.query.filter(texto.seccion == "hitos")
    return render_template('Admin/admin-hitos.html', title='Administracion - Textos', data = datos)

@main.route("/admin/hitos/update<int:id_txt>", methods=['GET', 'POST'])
def update_hito(id_txt):
    datos = texto.query.filter_by(id=id_txt).first()
    form = formtexto()
    if form.validate_on_submit():
        datos.titulo = form.titulo.data
        datos.seccion = form.seccion.data
        if form.subseccion.data == '' or form.subseccion.data == None:
            datos.subseccion = None
        else:
            datos.subseccion = form.subseccion.data
        datos.contenido = form.contenido.data
        db.session.commit()
        flash('El texto ha sido actualizado correctamente!', 'success')
        return redirect(url_for('.tablas'))
    elif request.method == 'GET':
        form.titulo.data = datos.titulo
        form.seccion.data = datos.seccion
        form.subseccion.data = datos.subseccion
        form.contenido.data = datos.contenido
    return render_template('Admin/admin-hitos-update.html', title='Administracion - Editar', form = form)

@main.route("/admin/fondos")
def fondos():
    fondos = fondo.query.all()
    return render_template('Admin/admin-fondos.html', title='Administracion - Fondos', data=fondos)

@main.route("/admin/fondos/update-texto-fondo<id_fondo>", methods=['GET', 'POST'])
def update_fondo(id_fondo):
    datos = texto.query.filter_by(titulo=id_fondo).first()
    form = formtexto()
    if form.validate_on_submit():
        datos.titulo = form.titulo.data
        datos.seccion = form.seccion.data
        if form.subseccion.data:
            datos.subseccion = form.subseccion.data
        else:
            datos.subseccion = None
        datos.contenido = form.contenido.data
        db.session.commit()
        flash('El texto del fonfo ha sido actualizado correctamente!', 'success')
        return redirect(url_for('.fondos'))
    elif request.method == 'GET':
        form.titulo.data = datos.titulo
        form.seccion.data = datos.seccion
        form.subseccion.data = datos.subseccion
        form.contenido.data = datos.contenido
    return render_template('Admin/admin-fondos-update.html', title='Administracion - Editar', form = form)

@main.route("/admin/fondos/nuevo-fondo", methods=['GET', 'POST'])
def add_fondos():
    form = formFondo()
    if form.validate_on_submit():
        nombre = form.nombre.data
        nemo = form.nemo.data
        ruta = form.ruta.data
        f = fondo(nombre_fondo=nombre.upper(), nemo=nemo.upper(), ruta_web=ruta)
        txt = texto(titulo=nemo.upper(), seccion=nombre.upper(), contenido='texto descriptivo')
        db.session.add(f)
        db.session.add(txt)
        db.session.commit()
        flash('Se añadio nuevo fondo')
        return redirect(url_for('.fondos'))
    return render_template('Admin/admin-add-fondo.html', title='Administracion - Fondos', form=form)

@main.route("/admin/borrar-fondo/<f_id>", methods=['POST'])
def delFondo(f_id):
    f = fondo.query.filter_by(id=f_id).first()
    txt = texto.query.filter_by(titulo=f.nemo).first()
    db.session.delete(txt)
    db.session.delete(f)
    db.session.commit()
    flash('Se ha eliminado el fondo', 'success')
    return redirect(url_for('.fondos'))

@main.route("/admin/usuarios")
def users():
    datos = User.query.all()
    return render_template('Admin/admin-usuarios.html', title='Administracion - Usuarios', data=datos)

@main.route("/admin/formulario")
def fomulario():
    formularios = formulario.query.filter_by(eliminado = 0).order_by(formulario.fecha.desc())
    mensajes = contacto.query.filter_by(eliminado = 0).order_by(contacto.fecha.desc())
    return render_template('Admin/admin-formulario.html', title='Administracion - Formulario', formularios=formularios, mensajes=mensajes)

@main.route("/admin/formulario/ver-formulario-<id>")
def fomulario_completo(id):
    formularios = formulario.query.filter_by(id= id , eliminado = 0).first()
    return render_template('Admin/admin-formulario-completo.html', title='Administracion - Formulario', d=formularios)

#********************************** Utilidades **********************************#
#********************************** Documentos **********************************#
@main.route("/admin/upload/<tipo>", methods=['GET', 'POST'])
def upload_doc(tipo):
    form = formArchivo()
    form.fondo.choices=[((f.id, f.nombre_fondo)) for f in fondo.query.all()]
    if tipo == 'fondos':
        form.tipo.choices=[((t.id, t.nombre)) for t in tipos_archivo.query.filter_by(tipo='fondo')]
        if form.validate_on_submit():
            if form.archivo.data:
                nombre = form.nombre.data
                aux1 = form.tipo.data
                aux2 = form.fondo.data
                fon = fondo.query.filter_by(id=aux2).first()
                tipo = tipos_archivo.query.filter_by(id=aux1).first()
                num = documento.query.filter_by(tipo=tipo.id).count()
                nom_arch = tipo.nom_doc+'-fondo_'+str(fon.id)+'-'+str(num)+'.pdf'
                enlace1 = bcrypt.generate_password_hash(nom_arch).decode('utf-8')
                aux_enl = enlace1.split('/')
                enlace2 = ''
                enlace = ''
                for a in aux_enl:
                    enlace2 += a
                aux_enl = enlace2.split('.')
                for a in aux_enl:
                    enlace += a
                
                if int(aux1) == 1 or int(aux1) == 3:
                    try:
                        aux_docs = documento.query.filter_by(nombre=nombre,tipo =aux1, fondo = fon.id, visible = 1)
                    except Exception:
                        aux_docs = None
                    if aux_docs:
                        for a in aux_docs:
                            a.visible = 0
                            db.session.commit()

                archivo = documento(nombre=nombre, tipo=tipo.id, fondo=fon.id, nom_int=nom_arch, link=('/descargar/'+enlace))
                db.session.add(archivo)
                db.session.commit()
                doc = form.archivo.data
                doc.filename = nom_arch
                doc_path = os.path.join(current_app.root_path, 'static/uploads/fondos', doc.filename)
                doc.save(doc_path)
            flash('Su archivo a sido subido exitosamente!', 'success')
            return redirect(url_for('.admin'))
    elif tipo == 'legal-agf':
        form.tipo.choices=[((t.id, t.nombre)) for t in tipos_archivo.query.filter_by(tipo='legal-agf')]
        if form.validate_on_submit():
            if form.archivo.data:
                nombre = form.nombre.data
                aux1 = form.tipo.data
                tipo = tipos_archivo.query.filter_by(id=aux1).first()
                num = documento.query.filter_by(tipo=tipo.id).count()
                nom_arch = tipo.nom_doc+'-'+str(num)+'.pdf'
                enlace1 = bcrypt.generate_password_hash(nom_arch).decode('utf-8')
                aux_enl = enlace1.split('/')
                enlace2 = ''
                enlace = ''
                for a in aux_enl:
                    enlace2 += a
                aux_enl = enlace2.split('.')
                for a in aux_enl:
                    enlace += a
                archivo = documento(nombre=nombre, tipo=tipo.id, fondo=None, nom_int=nom_arch, link=('/descargar/'+enlace))
                db.session.add(archivo)
                db.session.commit()
                doc = form.archivo.data
                doc.filename = nom_arch
                doc_path = os.path.join(current_app.root_path, 'static/uploads/legales/agf', doc.filename)
                doc.save(doc_path)
            flash('Su archivo a sido subido exitosamente!', 'success')
            return redirect(url_for('.admin'))
    elif tipo == 'legal-inv':
        form.tipo.choices=[((t.id, t.nombre)) for t in tipos_archivo.query.filter_by(tipo='legal-inv')]
        if form.validate_on_submit():
            if form.archivo.data:
                nombre = form.nombre.data
                aux1 = form.tipo.data
                tipo = tipos_archivo.query.filter_by(id=aux1).first()
                num = documento.query.filter_by(tipo=tipo.id).count()
                nom_arch = tipo.nom_doc+'-'+str(num)+'.pdf'
                enlace1 = bcrypt.generate_password_hash(nom_arch).decode('utf-8')
                aux_enl = enlace1.split('/')
                enlace2 = ''
                enlace = ''
                for a in aux_enl:
                    enlace2 += a
                aux_enl = enlace2.split('.')
                for a in aux_enl:
                    enlace += a
                archivo = documento(nombre=nombre, tipo=tipo.id, fondo=None, nom_int=nom_arch, link=('/descargar/'+enlace))
                db.session.add(archivo)
                db.session.commit()
                doc = form.archivo.data
                doc.filename = nom_arch
                doc_path = os.path.join(current_app.root_path, 'static/uploads/legales/inv', doc.filename)
                doc.save(doc_path)
            flash('Su archivo a sido subido exitosamente!', 'success')
            return redirect(url_for('.admin'))
    elif tipo == 'legal-cdb':
        form.tipo.choices=[((t.id, t.nombre)) for t in tipos_archivo.query.filter_by(tipo='legal-cdb')]
        if form.validate_on_submit():
            if form.archivo.data:
                nombre = form.nombre.data
                aux1 = form.tipo.data
                tipo = tipos_archivo.query.filter_by(id=aux1).first()
                num = documento.query.filter_by(tipo=tipo.id).count()
                nom_arch = tipo.nom_doc+'-'+str(num)+'.pdf'
                enlace1 = bcrypt.generate_password_hash(nom_arch).decode('utf-8')
                aux_enl = enlace1.split('/')
                enlace2 = ''
                enlace = ''
                for a in aux_enl:
                    enlace2 += a
                aux_enl = enlace2.split('.')
                for a in aux_enl:
                    enlace += a
                archivo = documento(nombre=nombre, tipo=tipo.id, fondo=None, nom_int=nom_arch, link=('/descargar/'+enlace))
                db.session.add(archivo)
                db.session.commit()
                doc = form.archivo.data
                doc.filename = nom_arch
                doc_path = os.path.join(current_app.root_path, 'static/uploads/legales/cdb', doc.filename)
                doc.save(doc_path)
            flash('Su archivo a sido subido exitosamente!', 'success')
            return redirect(url_for('.admin'))
    elif tipo == 'otros':
        form.tipo.choices=[((t.id, t.nombre)) for t in tipos_archivo.query.filter_by(tipo='otros')]
        if form.validate_on_submit():
            if form.archivo2.data:
                nombre = form.nombre.data
                aux1 = form.tipo.data
                tipo = tipos_archivo.query.filter_by(id=aux1).first()
                num = documento.query.filter_by(tipo=tipo.id).count()
                aux_ext = form.archivo2.data
                extension = aux_ext.filename
                extension = extension.split('.')[-1]
                nom_arch = tipo.nom_doc+'-'+str(num)+'.'+extension
                print(nom_arch)
                enlace1 = bcrypt.generate_password_hash(nom_arch).decode('utf-8')
                aux_enl = enlace1.split('/')
                enlace2 = ''
                enlace = ''
                for a in aux_enl:
                    enlace2 += a
                aux_enl = enlace2.split('.')
                for a in aux_enl:
                    enlace += a
                archivo = documento(nombre=nombre, tipo=tipo.id, fondo=None, nom_int=nom_arch, link=('/descargar/'+enlace))
                db.session.add(archivo)
                db.session.commit()
                doc = form.archivo2.data
                doc.filename = nom_arch
                doc_path = os.path.join(current_app.root_path, 'static/uploads/docs', doc.filename)
                doc.save(doc_path)
            flash('Su archivo a sido subido exitosamente!', 'success')
            return redirect(url_for('.docs'))
    else:
        return redirect(url_for('.upload_doc'))
    return render_template('Admin/admin-subir-doc.html', title='prueba', form=form, tipo=tipo)

@main.route("/admin/documentos")
def docs():
    documentos = []
    docs = documento.query.all()
    i=1
    for d in docs:
        aux = {}
        auxtip = d.tipo
        tip = tipos_archivo.query.filter_by(id=auxtip).first()
        seccion = tip.tipo
        nom_tipo = tip.nombre
        nombre = d.nombre
        link = d.link
        if d.visible == 1:
            visible = 1
        else:
            visible = None
        if d.fondo:
            f = fondo.query.filter_by(id = int(d.fondo)).first()
            nom_fondo = f.nombre_fondo
        else:
            nom_fondo = ''
        aux['num'] = i
        aux['nombre'] = nombre
        aux['tipo'] = nom_tipo
        aux['seccion'] = seccion
        aux['fondo'] = nom_fondo
        aux['link'] = link
        auxL = link.lstrip('/descargar/')
        aux['del_link'] = auxL
        aux['visible'] = visible
        i+=1

        documentos.append(aux)
    return render_template('Admin/admin-docs.html', title='Administracion', doc=documentos)

#********************************** Noticias **********************************#
@main.route("/admin/subir-noticia", methods=['GET', 'POST'])
def subirNoticia():
    form = formNoticia()
    if form.validate_on_submit():
        tipo = form.tipo.data
        aux = noticia.query.all()
        if aux:
            num = aux[-1].id
        else:
            num = 0
        if form.imagen.data:
            img = form.imagen.data
            aux = img.filename
            extension = aux.split('.')[1]
            img.filename = tipo+str(num+1)+'.'+extension
            img_path = os.path.join(current_app.root_path, 'static/uploads/noticias/img', img.filename)
            img.save(img_path)
            img_name = img.filename
        else:
            img_name = None
        if form.archivo.data:
            arch = form.archivo.data
            aux = arch.filename
            extension = aux.split('.')[1]
            arch.filename = tipo+str(num+1)+'.'+extension
            arch_path = os.path.join(current_app.root_path, 'static/uploads/noticias/doc', arch.filename)
            arch.save(arch_path)
            arch_name = arch.filename
        else:
            arch_name = None
        
        titulo = form.titulo.data
        bajada = form.bajada.data
        contenido = form.contenido.data
        fecha = form.fecha.data
        noti = noticia(tipo=tipo, titulo=titulo, bajada=bajada, contenido=contenido, fecha=fecha, imagen=img_name, archivo=arch_name)
        db.session.add(noti)
        db.session.commit()
        flash('Se ha agregado una nueva noticia', 'success')
        return redirect(url_for('.admin'))
    return render_template('Admin/admin-subir-noticia.html', title='Administracion', form = form, img='', arch='')

@main.route("/admin/noticias")
def verNoticias():
    datos = noticia.query.all()
    return render_template('Admin/admin-noticias.html', title='Administracion - Noticias', data = datos)

@main.route("/admin/editar-noticia/<n_id>", methods=['GET', 'POST'])
def editNoticia(n_id):
    form = formNoticia()
    dato = noticia.query.filter_by(id=n_id).first()
    if form.validate_on_submit():
        tipo = form.tipo.data
        dato.tipo = tipo
        dato.titulo = form.titulo.data
        dato.bajada = form.bajada.data
        dato.contenido = form.contenido.data
        dato.fecha = form.fecha.data
        if form.imagen.data:
            if dato.imagen:
                os.remove(os.path.join(current_app.root_path, 'static/uploads/noticias/img', dato.imagen))
            img = form.imagen.data
            aux = img.filename
            extension = aux.split('.')[1]
            img.filename = tipo+str(n_id)+'.'+extension
            img_path = os.path.join(current_app.root_path, 'static/uploads/noticias/img', img.filename)
            img.save(img_path)
            dato.imagen = img.filename

        if form.archivo.data:
            if dato.archivo:
                os.remove(os.path.join(current_app.root_path, 'static/uploads/noticias/doc', dato.archivo))
            arch = form.archivo.data
            aux = arch.filename
            extension = aux.split('.')[1]
            arch.filename = tipo+str(n_id)+'.'+extension
            arch_path = os.path.join(current_app.root_path, 'static/uploads/noticias/doc', arch.filename)
            arch.save(arch_path)
            dato.archivo = arch.filename

        db.session.commit()
        flash('Se ha actualizado la noticia', 'success')
        return redirect(url_for('.admin'))
    elif request.method == 'GET':
        form.tipo.data = dato.tipo
        form.titulo.data = dato.titulo
        form.bajada.data = dato.bajada
        form.contenido.data = dato.contenido
        form.fecha.data = dato.fecha
        img = ''
        arch = ''
        if dato.imagen:
            img = dato.imagen
        if dato.archivo:
            arch = dato.archivo
        
    return render_template('Admin/admin-subir-noticia.html', title='Administracion', form = form, img=img, arch=arch)

@main.route("/admin/borrar-noticia/<n_id>", methods=['POST'])
def delNoticia(n_id):
    noti = noticia.query.filter_by(id=n_id).first()
    if noti.imagen:
        os.remove(os.path.join(current_app.root_path, 'static/uploads/noticias/img', noti.imagen))
    if noti.archivo:
        os.remove(os.path.join(current_app.root_path, 'static/uploads/noticias/doc', noti.archivo))
    db.session.delete(noti)
    db.session.commit()
    flash('Se ha eliminado la noticia', 'success')
    return redirect(url_for('.verNoticias'))

@main.route("/<rutaaa>/descargar/<codigo>")
def descargar(codigo,rutaaa):
    print(codigo)
    cod = '/descargar/'+codigo
    doc = documento.query.filter_by(link=cod).first()
    tipo_doc = tipos_archivo.query.filter_by(id=doc.tipo).first()
    if tipo_doc.tipo == 'fondo':
        ruta='uploads/fondos/'+doc.nom_int
    if tipo_doc.tipo == 'legal-agf':
        ruta='uploads/legales/agf/'+doc.nom_int
    if tipo_doc.tipo == 'legal-cdb':
        ruta='uploads/legales/cdb/'+doc.nom_int
    if tipo_doc.tipo == 'otros':
        ruta='uploads/docs/'+doc.nom_int
    print(ruta)
    return render_template('Version-ES/documento.html', title='Contacto', ruta=ruta)

@main.route("/descargar/<codigo>")
def descargar2(codigo):
    print(codigo)
    cod = '/descargar/'+codigo
    doc = documento.query.filter_by(link=cod).first()
    tipo_doc = tipos_archivo.query.filter_by(id=doc.tipo).first()
    if tipo_doc.tipo == 'fondo':
        ruta='uploads/fondos/'+doc.nom_int
    if tipo_doc.tipo == 'legal-agf':
        ruta='uploads/legales/agf/'+doc.nom_int
    if tipo_doc.tipo == 'legal-cdb':
        ruta='uploads/legales/cdb/'+doc.nom_int
    if tipo_doc.tipo == 'legal-inv':
        ruta='uploads/legales/inv/'+doc.nom_int
    if tipo_doc.tipo == 'otros':
        ruta='uploads/docs/'+doc.nom_int
    print(ruta)
    return render_template('Version-ES/documento.html', title='Contacto', ruta=ruta)

@main.route("/borrar/<codigo>", methods=['GET', 'POST'])
def deletedoc(codigo):
    print(codigo)
    cod = '/descargar/'+codigo
    doc = documento.query.filter_by(link=cod).first()
    print(doc)
    tipo_doc = tipos_archivo.query.filter_by(id=doc.tipo).first()
    if tipo_doc.tipo == 'fondo':
        os.remove(os.path.join(current_app.root_path, 'static/uploads/fondos/', doc.nom_int))
    if tipo_doc.tipo == 'legal-agf':
        os.remove(os.path.join(current_app.root_path, 'static/uploads/legales/agf/', doc.nom_int))
    if tipo_doc.tipo == 'legal-cdb':
        os.remove(os.path.join(current_app.root_path, 'static/uploads/legales/cdb/', doc.nom_int))
    if tipo_doc.tipo == 'legal-inv':
        os.remove(os.path.join(current_app.root_path, 'static/uploads/legales/inv/', doc.nom_int))
    if tipo_doc.tipo == 'otros':
        os.remove(os.path.join(current_app.root_path, 'static/uploads/docs/', doc.nom_int))

    db.session.delete(doc)
    db.session.commit()
    flash('Se ha eliminado el documento', 'success')
    return redirect(url_for('.docs'))

@main.route('/w2/wp-content/uploads', defaults={'req_path': ''})
@main.route('/w2/wp-content/uploads/<path:req_path>')
def dir_listing(req_path):
    BASE_DIR = current_app.root_path+'/static/uploads/uploads/'

    # Joining the base and the requested path
    abs_path = os.path.join(BASE_DIR, req_path)

    # Return 404 if path doesn't exist
    if not os.path.exists(abs_path):
        print('no eexiste')
        return abort(404)

    # Check if path is a file and serve
    if os.path.isfile(abs_path):
        return send_file(abs_path)

    # Show directory contents
    files = os.listdir(abs_path)
    return render_template('files.html', files=files)

@main.route('/landing-corredora', defaults={'req_path': ''})
@main.route('/landing-corredora/<path:req_path>')
def dir_landing(req_path):
    BASE_DIR = current_app.root_path+'/static/uploads/landings/'

    # Joining the base and the requested path
    abs_path = os.path.join(BASE_DIR, req_path)

    # Return 404 if path doesn't exist
    if not os.path.exists(abs_path):
        return abort(404)

    # Check if path is a file and serve
    if os.path.isfile(abs_path):
        return send_file(abs_path)

    # Show directory contents
    files = os.listdir(abs_path)
    return render_template('files.html', files=files)

@main.route('/citas', defaults={'req_path': ''})
@main.route('/citas/<path:req_path>')
def dir_citas(req_path):
    BASE_DIR = current_app.root_path+'/static/uploads/citas/'

    # Joining the base and the requested path
    abs_path = os.path.join(BASE_DIR, req_path)

    # Return 404 if path doesn't exist
    if not os.path.exists(abs_path):
        return abort(404)

    # Check if path is a file and serve
    if os.path.isfile(abs_path):
        return send_file(abs_path)

    # Show directory contents
    files = os.listdir(abs_path)
    return render_template('files.html', files=files)